# e-NV200 daydriving CAN switch

The e-nv200 does not allow the driver to switch off the fron daydriving lights, since regulations require them to be always on when the car drives. Using this car as a camper, requires to switch on the hv system from time to time ... which turns on the daydriving lights.

This repo contains information and code to build a CAN interface to be able to switch the daydring lights out, while in park mode, and enables them automatically when leaving park mode.

## Manually switch off the light

As a interim solution, the relais that switches the two daydriving light circuits can be manually removed. The relais is located under the hood of the car, right next to the battery.

## Hardware

Currently works with the following devices:

* Arduino Nano

# Build

## Clone the repo

`git clone https://gitlab.com/steffenkockel/e-nv200-daydriving-light-can-switch.git`

## Install platformio

```
cd e-nv200-daydriving-light-can-switch
virtualenv .
. bin/activate
pip install platformio
```

## Build

```
pio run -e arduino_nano
```

```
pio run -e arduino_nano -t upload
```